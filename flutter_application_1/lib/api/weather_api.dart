import 'dart:convert';
import 'dart:developer';

import 'package:flutter_application_1/mosels/weather_forecast_daily.dart';
import 'package:flutter_application_1/utilities/constants.dart';
import 'package:http/http.dart' as http;

class WeatherApi {
  Future<WeatherForecast> fetchWeatherForecastWithCity(
      {String? cityName}) async {
    var queryParametrs = {
      'APPID': Constants.WEATHER_APP_ID,
      'units': 'metric',
      'q': cityName,
    };
    var uri = Uri.http(
      Constants.WEATHER_BASW_URL_DOMAIN,
      Constants.WEATHER_FORECAST_PATH,
      queryParametrs,
    );

    log('reauest: ${uri.toString()}');

    var response = await http.get(uri);

    print('response: ${response.body}');

    if (response.statusCode == 200) {
      return WeatherForecast.fromJson(json.decode(response.body));
    } else {
      throw Exception('Eror response');
    }
  }
}
