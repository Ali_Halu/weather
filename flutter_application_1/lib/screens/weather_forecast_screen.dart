import 'package:flutter/material.dart';
import 'package:flutter_application_1/api/weather_api.dart';
import 'package:flutter_application_1/mosels/weather_forecast_daily.dart';
import 'package:flutter_application_1/screens/sity_screen.dart';
import 'package:flutter_application_1/widgets/botton_list_view.dart';
import 'package:flutter_application_1/widgets/detail_view.dart';
import 'package:flutter_application_1/widgets/sity_view.dart';
import 'package:flutter_application_1/widgets/temp_view.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class WeaterForecastScreen extends StatefulWidget {
  const WeaterForecastScreen({Key? key}) : super(key: key);

  @override
  _WeaterForecastScreenState createState() => _WeaterForecastScreenState();
}

class _WeaterForecastScreenState extends State<WeaterForecastScreen> {
  late Future<WeatherForecast> forcastObject;
  late String _cityName = 'Moscow';
  @override
  void initState() {
    super.initState();
    forcastObject =
        WeatherApi().fetchWeatherForecastWithCity(cityName: _cityName);
    // forcastObject.then((Weather) {
    //   print(Weather.list![0].weather[0].main);
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text('Weather'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.my_location),
          onPressed: () {},
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.location_city),
              onPressed: () async {
                var tappedName = await Navigator.push(context,
                    MaterialPageRoute(builder: (context) {
                  return CityScreen();
                }));
                if (tappedName != null) {
                  _cityName = tappedName;
                  forcastObject = WeatherApi()
                      .fetchWeatherForecastWithCity(cityName: _cityName);
                }
              }),
        ],
      ),
      body: ListView(children: [
        Container(
          child: FutureBuilder<WeatherForecast>(
            future: forcastObject,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                  children: [
                    SizedBox(height: 50),
                    CityView(snapshot: snapshot),
                    SizedBox(height: 50),
                    Tempview(snapshot: snapshot),
                    SizedBox(height: 50),
                    DetailView(snapshot: snapshot),
                    SizedBox(height: 50),
                    BottonListView(snapshot: snapshot),
                  ],
                );
              } else {
                return Center(
                  child: SpinKitDoubleBounce(
                    color: Colors.black87,
                    size: 100.0,
                  ),
                );
              }
            },
          ),
        )
      ]),
    );
  }
}
