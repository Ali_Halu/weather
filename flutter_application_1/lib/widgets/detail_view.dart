import 'package:flutter/material.dart';
import 'package:flutter_application_1/mosels/weather_forecast_daily.dart';
import 'package:flutter_application_1/utilities/forcast_util.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DetailView extends StatelessWidget {
  final AsyncSnapshot<WeatherForecast> snapshot;
  const DetailView({required this.snapshot});
  @override
  Widget build(BuildContext context) {
    var forrecastList = snapshot.data!.list;
    var pressure = forrecastList![0].pressure * 0.750062;
    var humidity = forrecastList[0].humidity;
    var wind = forrecastList[0].speed;

    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        children: [
          Util.getItem(FontAwesomeIcons.thermometerThreeQuarters,
              pressure.round(), 'mm,Hg'),
          Util.getItem(FontAwesomeIcons.cloudRain, humidity, '%'),
          Util.getItem(FontAwesomeIcons.wind, wind.toInt(), 'm/s'),
        ],
      ),
    );
  }
}
