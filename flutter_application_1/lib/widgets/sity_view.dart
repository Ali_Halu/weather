import 'package:flutter/material.dart';
import 'package:flutter_application_1/mosels/weather_forecast_daily.dart';
import 'package:flutter_application_1/utilities/forcast_util.dart';

class CityView extends StatelessWidget {
  final AsyncSnapshot<WeatherForecast> snapshot;
  const CityView({required this.snapshot});

  @override
  Widget build(BuildContext context) {
    var forrecastList = snapshot.data!.list;
    var city = snapshot.data!.city.name;
    var country = snapshot.data!.city.country;
    var formatedDate =
        DateTime.fromMillisecondsSinceEpoch(forrecastList![0].dt * 1000);

    return Container(
      child: Column(
        children: [
          Text(
            '$city, $country',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 28,
                color: Colors.black87),
          ),
          Text(
            '${Util.getFormatedDate(formatedDate)}',
            style: TextStyle(fontSize: 15),
          )
        ],
      ),
    );
  }
}
