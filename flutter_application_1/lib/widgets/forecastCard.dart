import 'package:flutter/material.dart';
import 'package:flutter_application_1/utilities/forcast_util.dart';

Widget forecastCard(AsyncSnapshot snapshot, int index) {
  var forrecastList = snapshot.data!.list;
  DateTime date =
      DateTime.fromMillisecondsSinceEpoch(forrecastList[index].dt * 1000);
  var fullDate = Util.getFormatedDate(date);
  var dayOfWeek = '';
  dayOfWeek = fullDate.split(',')[0];
  var tempMin = forrecastList[index].temp.min.toStringAsFixed(0);
  var icon = forrecastList[index].getIconUrl();

  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Center(
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Text(
            dayOfWeek,
            style: TextStyle(fontSize: 25, color: Colors.white),
          ),
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Text(
                      '$tempMin °C',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                  ),
                  Image.network(
                    icon,
                    scale: 1.2,
                    color: Colors.white,
                  )
                ],
              ),
            ],
          )
        ],
      )
    ],
  );
}
