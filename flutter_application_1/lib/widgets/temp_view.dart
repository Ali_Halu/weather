import 'package:flutter/material.dart';
import 'package:flutter_application_1/mosels/weather_forecast_daily.dart';

class Tempview extends StatelessWidget {
  final AsyncSnapshot<WeatherForecast> snapshot;
  const Tempview({required this.snapshot});

  @override
  Widget build(BuildContext context) {
    var forrecastList = snapshot.data!.list;
    var icon = forrecastList![0].getIconUrl();
    var temp = forrecastList[0].temp.day.toStringAsFixed(0);
    var description = forrecastList[0].weather[0].description.toUpperCase();
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.network(
            icon,
            scale: 0.4,
            color: Colors.black87,
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            children: [
              Text(
                '$temp °C',
                style: TextStyle(fontSize: 54.0, color: Colors.black87),
              ),
              Text('$description',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black87,
                  ))
            ],
          )
        ],
      ),
    );
  }
}
